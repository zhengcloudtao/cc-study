#include <string.h>
#include "stdio.h"

int main(){
    char c[20]={'t','m','o'};
    char str[3]={'a','b','c'};

    printf("%s",c);
    printf("\n%s",str);

    char c1[]="hello";
    printf("\n%s",c1);

    char greeting[]="Hello";

    int len=strlen(greeting);

    for(int i=0;i<len;i++){
        printf("\n%c",greeting[i]);
    }


    char *pStr="helloTom";
    printf("\npStr=%s",pStr);

    char *a="yes";
    printf("\na本身的地址=%p a指向的地址=%p",&a,a);
    a="hello tom";
    printf("\na本身的地址=%p a指向的地址=%p",&a,a);
    printf("\na=%s",a);
}