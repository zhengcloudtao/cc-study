# include "stdio.h"
#define PI 3.134
const double P=3.14;
#define A 1
#define B (A+3)
#define C A/B*3
#define PI2
#undef PI2
int main(){

    int n1=0213;
    int n2=0X4b;
    char c1='a';
    char c2='\t';

    char str1[20]="北京hello";
    char str2[100]="hello world";
    //define 没数据类型  预处理生效
    double a= PI * 2.0;
    printf("%.2f",a);
    printf("\nn1=%d n2=%d",n1,n2);
    printf("\nstr=%s str2=%s",str1,str2);
    //const 有数据类型 编译生效
    printf("\n%.2f",P);

    printf("\nc=%.2f",C);

    getchar();

}