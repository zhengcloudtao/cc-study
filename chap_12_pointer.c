#include "stdio.h"

int main() {
    int num1 = 1;
    int *ptr = &num1;
    printf("num的值=%d num地址=%p", num1, &num1);
    printf("\nptr的地址=%p ptr存放的值是一个地址为=%p ptr指向的值=%d", &ptr, ptr, *ptr);

    //
    int num = 88;
    printf("\nnum的值=%d num的地址=%p", num, &num);

    int *ptr1 = &num;
    *ptr1 = 99;
    printf("\nnum的值=%d num的地址=%p", num, &num);

    //应用案例
    int a = 300;
    int b = 400;
    ptr = &a;
    *ptr = 100;
    ptr = &b;
    *ptr = 200;
    printf("\na=%d,b=%d", a, b);


    //地址传递
    num=100;
    int *p=&num;
    int *p2=p;
    *p2=55;
    printf("\nnum=%d *p=%d *p2=%d",num,*p,*p2);


}