#include "stdio.h"
void fn_static(void){
    static int n=10;
    printf("n=%d\n",n);
    n++;
    printf("n++=%d\n",n);
}
int main(){
    fn_static();
    fn_static();
}