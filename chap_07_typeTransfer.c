#include "stdio.h"

int main() {
    char c1 = 'a';
    int num1 = c1;
    double d1 = num1;

    short s1 = 10;
    int num2 = 20;
    int num3 = num2 + s1;

    float f1 = 1.1f;
    double d2 = 4.12312314124124;
    f1 = d2;
    printf("f1=%.8f", f1);
}
