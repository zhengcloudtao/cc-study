#include "stdio.h"

int main() {
    double d1 = 10 / 4;
    double d2 = 10.0 / 4;

    printf("d1=%f d2=%f", d1, d2);

    int res1 = 10 % 3;
    int res2 = -10 % 3;
    int res3 = 10 % -3;
    int res4 = -10 % -3;
    printf("\nres1=%d res2=%d res3=%d res4=%d", res1, res2, res3, res4);

    int i=10;
    int j=i++;
    int k=++i;

    printf("\ni=%d j=%d k=%d",i,j,k);

    k++;
    ++k;
    printf("\nk=%d",k);



}